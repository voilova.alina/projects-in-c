This programm allows you to calculate simple expressions using the shunting yard algorithm. It uses postfix notation (also known as Reverse Polish notation).
This calculator implements not only summation and subtraction, but also division, multiplication and exponentiation.

You can read more about the shunting yard algorithm at: https://en.wikipedia.org/wiki/Shunting-yard_algorithm
Also: https://www.youtube.com/watch?v=y_snKkv0gWc

Note: to calculate expressions don't use any spaces between numbers
for example, "2*(4-4)" instead of "2 * (4 + 4)"
